        MOVI 7,0X8000
        MOVI 1,-32767
        MOVI 2,-32766

        BEQ 1,2,FALSE //1 = 2 so not greater
        
        NAND 3,2,2    //two's complement
        ADDI 3,3,1

        ADD 4,1,3

        NAND 4,7,4    //isolate the msb
        NAND 4,4,4

        BEQ 4,0,TRUE  //msb is 0 so 1 - 2 is positive then 1>2
FALSE:  ADDI 3,0,0    //no jump so 1 - 2 is negative then 1< 2
        BEQ 0,0,END

TRUE:   ADDI 3,0,1

END:    HALT


