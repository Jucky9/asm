        MOVI 2,0        //will be removed by the Online Verification Tool
        NOP             //will be removed by the Online Verification Tool
        MOVI 1,0        //will be removed by the Online Verification Tool
        NOP             //will be removed by the Online Verification Tool

        MOVI 6,0X8000   //mask to isolate the MSB
        NOP
        NAND 7,6,1      //apply the mask
        NAND 7,7,7
        BEQ 7,0,B2

        NAND 7,7,7
FOR:    BEQ 2,0,END  //if the value in reg 1 has a MSB of 1
        ADDI 2,2,-1
        NAND 5,3,6
        ADD 3,3,1
        BEQ 5,7,INC  //both have a MSB of 1
        NAND 5,3,6
        BEQ 5,7,FOR  //the MSB was 1 and still is 1
INC:    ADDI 4,4,1
        BEQ 0,0,FOR

B2:     NAND 7,7,7 
FOR2:   BEQ 2,0,END   //if the value in reg 1 has a MSB of 0
        ADDI 2,2,-1
        NAND 5,3,6
        ADD 3,3,1
        BEQ 5,7,FOR2  //both have a MSB of 0
        NAND 5,3,6
        BEQ 5,7,INC2  //overflow because before MSB was 1 in reg3 and now it is 0
        BEQ 0,0,FOR2
INC2:   ADDI 4,4,1
        BEQ 0,0,FOR2
END:    HALT
